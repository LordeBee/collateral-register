import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  // private baseUrl =  "https://ghuatgodigisrv1.gh.sbicdirectory.com:7102/";
  // private baseUrl = '/api/';

  // private accinfoUrl = this.baseUrl + "AccountServices/accounts/customeraccount?cif=";
  // private loginUrl = this.baseUrl + "AuthServices/Staff/ldapauthentication";
  // private insinfoUrl = this.baseUrl + "CollateralServices/services/collateraladd";
  // private colInsUrl = this.baseUrl + "CollateralServices/services/collaterals/insurances?type=status&status=";
  // private colValUrl = this.baseUrl + "CollateralServices/services/collaterals/valuations?type=status&status=";
  // private colActInsUrl = this.baseUrl + "CollateralServices/services/collaterals/insurances?type=action&status=";
  // private insIdUrl = this.baseUrl + "CollateralServices/services/collateral/insurance?id=";
  // private valIdUrl = this.baseUrl + "CollateralServices/services/collateral/valuation?id=";
  // private colUpdUrl = this.baseUrl + "CollateralServices/services/collateralupdate";

  private baseUrl =  "https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/collateral/api/";
  private accinfoUrl = this.baseUrl + "account_info";
  private loginUrl = this.baseUrl + "user_login";
  private insinfoUrl = this.baseUrl + "add_col";
  private colInsUrl = this.baseUrl + "ins_status";
  private colValUrl = this.baseUrl + "val_status";
  private colActInsUrl = this.baseUrl + "ins_action";
  private insIdUrl = this.baseUrl + "ins_id";
  private valIdUrl = this.baseUrl + "val_id";
  private colUpdUrl = this.baseUrl + "upd_col";
  private curUrl = this.baseUrl + "get_currency";
  private accinfoNoUrl = this.baseUrl + "account_info_no";


  private httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Access-Control-Allow-Origin', '*')
  .set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
  .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
  .set('sourceCode','Collateral')
  .set('countryCode','GH');
 
  private options = {
   headers: this.httpHeaders
 };

  constructor(private _http:HttpClient) { }

  account_info(data:any){
 
    return this._http.post(this.accinfoUrl,data, this.options);
  }

  account_info_no(data:any){
 
    return this._http.post(this.accinfoNoUrl,data, this.options);
  }

  user_login(data:any){
    return this._http.post(this.loginUrl, data, this.options);
  }


  ins_sub(data:any){
    return this._http.post(this.insinfoUrl, data, this.options);
  }


  ins_info(data:any){
    return this._http.post(this.colInsUrl,data ,this.options); 
  }

  act_ins_info(data:any){
    return this._http.post(this.colActInsUrl,data ,this.options); 
  }
 
  val_info(data:any){
    return this._http.post(this.colValUrl,data ,this.options); 
  }

  ins_id(data:any){
    return this._http.post(this.insIdUrl,data ,this.options); 
  }

  val_id(data:any){
    return this._http.post(this.valIdUrl,data ,this.options); 
  }

  col_updata(data){
    return this._http.post(this.colUpdUrl, data, this.options);
  }


  get_currency(){
    return this._http.post(this.curUrl, this.options);
  }




 

 
}
