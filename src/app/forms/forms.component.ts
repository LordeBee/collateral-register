import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { Router } from '@angular/router';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  start=true;
  formIns:any;
  submiting=false;
  username:any;
  formCol:any;
  formVal:any;
  options=true;
  account=false;
  insurance=false;
  valuation=false;
  ciftxt=true;
  loading=false;
  formData:any;
  formInfo:any;
  accountStep=false;
  insuranceStep=false;
  valuationStep=false;
  resp:any;
  response:any;
  accData:any;
  resp2:any;
  currency:any;
  fullname:any;
  accountType:any;
  formInfoNo:any;
  accresp:any;
  

  constructor(private router: Router,private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
    if (!this.formData) {
      this.formData = {
        type: ''		
       }
      }

      if (!this.formInfo) {
        this.formInfo = {
          cif: ''		
         }
        }

        if (!this.formInfoNo) {
          this.formInfoNo = {
            accountNumber: ''		
           }
          }
  
        if (!this.formCol) {
          this.formCol ={
            hostHeaderInfo:{
            requestId:"C12345",
            ipAddress:"0.0.0.0",
            sourceChannelId:"WEB"
            },
            insuranceInfo:{
              accountNumber:'',
              customerName:'',
              customerPhone:'',
              customerEmail:'',
              currency:'',
              segment:'',
              rm:'',
              cif:'',
              rmCode:'',
              insurer:'',
              insurerCover:'',
              policyNo:'',
              annualPremium:'',
              dateIssued:'',
              dateExipred:'',
              reInstateValue:'',
              thirdParty:'',
              ccReinstate:'',
              policyScheduled:'',
              comments:'',
              action:'',
              createdBy:''
            }
           }
           
    
    
        if (!this.formVal) {
            this.formVal ={
              hostHeaderInfo:{
              requestId:"C12345",
              ipAddress:"0.0.0.0",
              sourceChannelId:"WEB"
              },
              valuationInfo:{
                accountNumber:'',
                customerName:'',
                customerPhone:'',
                cif:'',
                customerEmail:'',
                valCcy:'',
                segment:'',
                rm:'',
                rmCode:'',
                propertyOwner:'',
                propertyDetails:'',
                valuer:'',
                omw:'',
                fsv:'',
                lcyFsv:'',
                riv:'',
                valuationDate:'',
                nextValuationDate:'',
                comments:'',
                createdBy:''
              }
             
            } 
          }
        }

        if (!this.formIns) {
          this.formIns ={
            hostHeaderInfo:{
            requestId:"C12345",
            ipAddress:"0.0.0.0",
            sourceChannelId:"WEB"
            },
            insuranceInfo:{
              accountNumber:'',
              customerName:'',
              customerPhone:'',
              customerEmail:'',
              currency:'',
              segment:'',
              rm:'',
              cif:'',
              rmCode:'',
              insurer:'',
              insurerCover:'',
              policyNo:'',
              annualPremium:'',
              dateIssued:'',
              dateExipred:'',
              reInstateValue:'',
              thirdParty:'',
              ccReinstate:'',
              policyScheduled:'',
              comments:'',
              action:'',
              createdBy:''
            },
            valuationInfo:{
              accountNumber:'',
              customerName:'',
              customerPhone:'',
              cif:'',
              customerEmail:'',
              valCcy:'',
              segment:'',
              rm:'',
              rmCode:'',
              propertyOwner:'',
              propertyDetails:'',
              valuer:'',
              omw:'',
              fsv:'',
              lcyFsv:'',
              riv:'',
              valuationDate:'',
              nextValuationDate:'',
              comments:'',
              createdBy:''
            }
           
          } 
        }
      
        
        
  }

  ngOnInit() {

    // this.getCurrency();

    var key ="collateralUserObj";
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(!userInfo){
      window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
    }
    if(userInfo != null){
      this.fullname = userInfo.fullname;
      this.username = userInfo.username;
      }
    $("#prev").hide();
    $("#next").hide();
    $("#sub").hide();
  }


  // accountChecker(accData:any) : boolean{
  //   var response = false;

  //   var count = accData.length;

  //   for (var i = 0; i < count; i++){
  //     response = ((accData.acountDesc[i].includes("CURRENT")) && (accData.bookBalance[i] > 0));
  //     if(response==true){
  //       break;
  //     }
  //     else if((response==false)&&(accData[i].length!= count)){
  //       continue;
  //     }

  //   }
  //   return response;
  // }

  


  accInfo(){

    if(this.formData.type===''){
      toast("Please Select a form",3000);

    }
    else{
      if(this.formInfo.cif===''){
        toast("Please enter a CIF",3000);
      }

      else{
        this.loading = true;
        // alert(this.formInfo.cif); 
        var info = JSON.stringify(this.formInfo);
        this.api_service.account_info(info).subscribe(res=>{
          this.resp = res;
         // console.log(this.resp);

          if(this.resp.hostHeaderInfo.responseCode == "000"){	
            this.formCol.insuranceInfo.customerEmail = this.resp.emailInfo[0].email;
            this.formCol.insuranceInfo.createdBy = this.username;
            this.formCol.insuranceInfo.customerPhone = this.resp.phoneInfo[0].phoneNumber;
            this.formCol.insuranceInfo.currency =  this.resp.accountsInfo[0].currencyCode;
            this.formCol.insuranceInfo.segment = this.resp.customerInfo.segment;
            this.formCol.insuranceInfo.rm = this.resp.relationManagerInfo.rmName;
            this.formCol.insuranceInfo.rmCode = this.resp.relationManagerInfo.rmCode;
            this.accData = this.resp.accountsInfo;
            var count = this.resp.accountsInfo.length;

            var state = this.checkAccount();

            if(state==true){   
              this.toAccountStep();
            }

            else if(state==false){
              toast("Select account number",3000);
              this.toAccountStep();
            }

           
          }

          else{
            this.loading = false;	
            toast(this.resp.hostHeaderInfo.responseMessage ,3000);
          
          }
        }, error => {
          this.loading = false; 
          toast("Could not load process at this time" ,3000);
        });
      }

     

    }
  }
  
  

  dashboard(){
    this.router.navigate(['/dashboard']);
  }

  

  logout(){
    var key ="collateralUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }


  prevStep(){
    if(this.formData.type=='Insurance'){

      if(this.accountStep==true){
        this.start = true;
        this.account = false;
        this.insurance = false;
        this.valuation = false;
        this.options = true;
        this.ciftxt = true;
        this.accountStep = false;
        this.insuranceStep = false;
        this.loading = false;
        $("#prog").css("width", "33%");
        $("#sub").hide();
        $("#prev").hide();
        $("#next").hide();
      }
      
      else if((this.insuranceStep==true)&&(this.accountStep==false)){
        this.start = false;
        this.account = true;
        this.insurance = false;
        this.valuation = false;
        this.options = false;
        this.ciftxt = false;
        this.accountStep = true;
        this.insuranceStep=false;
        $("#prog").css("width", "60%");
        $("#sub").hide();
        $("#prev").show();
        $("#next").show();

      }

    }

    else if(this.formData.type=='Valuation'){

      if(this.accountStep==true){
        this.start = true;
        this.account = false;
        this.insurance = false;
        this.valuation = false;
        this.options = true;
        this.ciftxt = true;
        this.accountStep = false;
        this.valuationStep = false;
        this.loading = false;
        $("#prog").css("width", "33%");
        $("#sub").hide();
        $("#prev").hide();
        $("#next").hide();
      }
      
      else if((this.valuationStep==true)&&(this.accountStep==false)){
        this.start = false;
        this.account = true;
        this.insurance = false;
        this.valuation = false;
        this.options = false;
        this.ciftxt = false;
        this.accountStep = true;
        this.valuationStep=false;
        $("#prog").css("width", "60%");
        $("#prev").show();
        $("#sub").hide();
        $("#next").show();

      }

    }

    else if(this.formData.type=='Ins_Val'){

      
      if(this.accountStep==true){
        this.start = true;
        this.account = false;
        this.insurance = false;
        this.valuation = false;
        this.options = true;
        this.ciftxt = true;
        this.accountStep = false;
        this.valuationStep = false;
        this.loading = false;
        $("#prog").css("width", "33%");
        $("#sub").hide();
        $("#prev").hide();
        $("#next").hide();
      }

      else if((this.insuranceStep==true)&&(this.accountStep==false)){
        this.start = false;
        this.account = true;
        this.insurance = false;
        this.valuation = false;
        this.options = false;
        this.ciftxt = false;
        this.accountStep = true;
        this.insuranceStep=false;
        $("#prog").css("width", "50%");
        $("#sub").hide();
        $("#prev").show();
        $("#next").show();

      }

      else if(this.valuationStep==true){
        
        this.start = false;
        this.account = false;
        this.insurance = true;
        this.valuation = false;
        this.options = false;
        this.ciftxt = false;
        this.accountStep = false;
        this.insuranceStep = true;
        this.valuationStep=false;
        $("#prog").css("width", "75%");
        $("#sub").hide();
        $("#prev").show();
        $("#next").show();

      }

    }
  }

  nextStep(){
    if(this.formData.type=='Insurance'){
      
      this.start = false;
      this.account = false;
      this.insurance = true;
      this.valuation = false;
      this.options = false;
      this.ciftxt = false;
      this.accountStep = false;
      this.insuranceStep = true;
      $("#sub").show();
      $("#prog").css("width", "100%");
      $("#prev").show();
      $("#next").hide();

      

    }

    else if(this.formData.type=='Valuation'){

      this.start = false;
      this.account = false;
      this.insurance = false;
      this.valuation = true;
      this.options = false;
      this.ciftxt = false;
      this.accountStep = false;
      this.valuationStep = true;
      $("#sub").show();
      $("#prog").css("width", "100%");
      $("#prev").show();
      $("#next").hide();

      

    }

    else if(this.formData.type=='Ins_Val'){

      if(this.accountStep==true){


        this.start = false;
        this.account = false;
        this.insurance = true;
        this.valuation = false;
        this.options = false;
        this.ciftxt = false;
        this.accountStep = false;
        this.insuranceStep = true;
        $("#prog").css("width", "75%");
        $("#prev").show();
        $("#next").show();

      }

      else if((this.insuranceStep==true) && (this.accountStep==false)){

        var result = $(".insuranceStep input[required]").filter(function () {
          return $.trim($(this).val()).length == 0
        }).length == 0;
      
        if(result==false){
          toast("Please fill al required fields",3000)
        }

        else{

          
        this.start = false;
        this.account = false;
        this.insurance = false;
        this.valuation = true;
        this.options = false;
        this.ciftxt = false;
        this.accountStep = false;
        this.valuationStep = true;
        this.insuranceStep = false;
        $("#prog").css("width", "100%");
        $("#sub").show();
        $("#prev").show();
        $("#next").hide();



        }

        


      }

    }
  }

  
  getData(){
    if((this.formCol.insuranceInfo.insurerCover!='')&&(this.formCol.insuranceInfo.reInstateValue!='')){
      var a = parseFloat(this.formCol.insuranceInfo.insurerCover);
      var b = parseFloat(this.formCol.insuranceInfo.reInstateValue);
      var c = a-b;
      this.formCol.insuranceInfo.ccReinstate = c;


    }
   
  }

  getccData(){
    if((this.formCol.insuranceInfo.insurerCover!='')&&(this.formCol.insuranceInfo.reInstateValue!='')){
      var a = parseFloat(this.formCol.insuranceInfo.insurerCover);
      var b = parseFloat(this.formCol.insuranceInfo.reInstateValue);
      var c = a-b;
      this.formCol.insuranceInfo.ccReinstate = c;
  

    }
   
  }

  send_ins(ins){
     
    this.api_service.ins_sub(ins).subscribe(res=>{
      this.response = res;
      if(this.response.hostHeaderInfo.responseCode == "000"){
        toast("Submitted Successfully",3000);           
        this.router.navigate(['/dashboard']);
      }
      else{
    
      toast("Sorry, Couldn't submit at this time",3000);
      }
      }, error => {
        toast("Sorry, Couldn't submit at this time",3000);
  
     
    });
  }

  sub(){
    
    if(this.formData.type=='Insurance'){
     
      var a = parseFloat(this.formCol.insuranceInfo.insurerCover);
      var b = parseFloat(this.formCol.insuranceInfo.reInstateValue);
      var c = a-b;
      this.formCol.insuranceInfo.ccReinstate = c;
      this.formCol.insuranceInfo.cif = this.formInfo.cif;
      var info = JSON.stringify(this.formCol);
      // console.log(info);
      


      var result = $(".insuranceStep input[required]").filter(function () {
        return $.trim($(this).val()).length == 0
      }).length == 0;
    
      if(result==false){
        toast("Please fill al required fields",3000)
      }
     else{
     

      if(c>=0){
        this.submiting = true;
        $("#sub").hide();
        $("#prev").hide();


      
      this.api_service.ins_sub(info).subscribe(res=>{
        this.response = res;
        if(this.response.hostHeaderInfo.responseCode == "000"){
          toast("Submitted Successfully",3000);       
          this.router.navigate(['/dashboard']);    
        }
        else{

        this.submiting = false;
        $("#sub").show();
        $("#prev").show();
      
        toast("Sorry, Couldn't submit at this time",3000);
        }
        }, error => {

        this.submiting = false;
        $("#sub").show();
        $("#prev").show();
        toast("Sorry, Couldn't submit at this time",3000);
    
       
      });
 
        
      }
      else if(c<0){
        this.submiting = true;
        $("#sub").hide();
        $("#prev").hide();

      this.formCol.insuranceInfo.action = 'Exception';
      var info = JSON.stringify(this.formCol);
      // alert(info);
      this.api_service.ins_sub(info).subscribe(res=>{
        this.response = res;
        if(this.response.hostHeaderInfo.responseCode == "000"){
          toast("Submitted Successfully",3000);       
          this.router.navigate(['/dashboard']);    
        }
        else{

        this.submiting = false;
        $("#sub").show();
        $("#prev").show();
      
        toast("Sorry, Couldn't submit at this time",3000);
        }
        }, error => {

        this.submiting = false;
        $("#sub").show();
        $("#prev").show();
        toast("Sorry, Couldn't submit at this time",3000);
    
       
      });
      }
     }
     

    }

    else if(this.formData.type=='Valuation'){

      


      this.formVal.valuationInfo.cif = this.formInfo.cif;
      this.formVal.valuationInfo.accountNumber = this.formCol.insuranceInfo.accountNumber;
      this.formVal.valuationInfo.customerName = this.formCol.insuranceInfo.customerName;
      this.formVal.valuationInfo.customerPhone = this.resp.phoneInfo[0].phoneNumber;
      this.formVal.valuationInfo.customerEmail = this.resp.emailInfo[0].email;
      this.formVal.valuationInfo.segment = this.resp.customerInfo.segment;
      this.formVal.valuationInfo.rm = this.resp.relationManagerInfo.rmName;
      this.formVal.valuationInfo.rmCode = this.resp.relationManagerInfo.rmCode;
      this.formVal.valuationInfo.createdBy = this.username;

      if((this.formVal.valuationInfo.propertyDetails==='')||
      (this.formVal.valuationInfo.valuer==='')||
      (this.formVal.valuationInfo.valCcy==='')||
      (this.formVal.valuationInfo.omw==='')||
      (this.formVal.valuationInfo.fsv==='')||
      (this.formVal.valuationInfo.lcyFsv==='')||
      (this.formVal.valuationInfo.valuationDate==='')||
      (this.formVal.valuationInfo.nextValuationDate==='')||
      (this.formVal.valuationInfo.nextValuationDate==='')){
        toast("Please fill all required fields",3000);
      }

      else{

        this.submiting = true;
  
        var data = JSON.stringify(this.formVal);
        this.api_service.ins_sub(data).subscribe(res=>{
          $("#sub").hide();
          $("#prev").hide();
          this.response = res;
          if(this.response.hostHeaderInfo.responseCode == "000"){
            toast("Submitted Successfully",3000);    
            this.router.navigate(['/dashboard']);       
          }
          else{
            this.submiting = false;
            $("#sub").show();
            $("#prev").show();
        
            toast(this.response.hostHeaderInfo.responseMessage,3000);
          }
          }, error => {
            this.submiting = false;
            $("#sub").show();
            $("#prev").show();
            toast("Sorry, Couldn't submit at this time",3000);
      
         
        });
   
  

      }

     

    }

    else if(this.formData.type=='Ins_Val'){

    
      var a = parseFloat(this.formCol.insuranceInfo.insurerCover);
      var b = parseFloat(this.formCol.insuranceInfo.ccReinstate);
      var c = a-b;

      this.formCol.insuranceInfo.reInstateValue = c;
      this.formIns.insuranceInfo.accountNumber = this.formCol.insuranceInfo.accountNumber;
      this.formIns.insuranceInfo.customerName = this.formCol.insuranceInfo.customerName;
      this.formIns.insuranceInfo.customerPhone = this.resp.phoneInfo[0].phoneNumber;
      this.formIns.insuranceInfo.customerEmail = this.resp.emailInfo[0].email;
      this.formIns.insuranceInfo.segment = this.resp.customerInfo.segment;
      this.formIns.insuranceInfo.rm = this.resp.relationManagerInfo.rmName;
      this.formIns.insuranceInfo.rmCode = this.resp.relationManagerInfo.rmCode;
      this.formIns.insuranceInfo.insurer = this.formCol.insuranceInfo.insurer;
      this.formIns.insuranceInfo.insurerCover = this.formCol.insuranceInfo.insurerCover;
      this.formIns.insuranceInfo.annualPremium = this.formCol.insuranceInfo.annualPremium;
      this.formIns.insuranceInfo.dateIssued = this.formCol.insuranceInfo.dateIssued;
      this.formIns.insuranceInfo.dateExipred = this.formCol.insuranceInfo.dateExipred;
      this.formIns.insuranceInfo.reInstateValue = this.formCol.insuranceInfo.reInstateValue;
      this.formIns.insuranceInfo.ccReinstate = this.formCol.insuranceInfo.ccReinstate;
      this.formIns.insuranceInfo.policyScheduled = this.formCol.insuranceInfo.policyScheduled;
      this.formIns.insuranceInfo.policyNo = this.formCol.insuranceInfo.policyNo;
      this.formIns.insuranceInfo.thirdParty = this.formCol.insuranceInfo.thirdParty;
      this.formIns.insuranceInfo.comments = this.formCol.insuranceInfo.comments;
      this.formIns.insuranceInfo.cif = this.formInfo.cif;
      this.formIns.insuranceInfo.createdBy = this.username;



      this.formIns.valuationInfo.cif = this.formInfo.cif;
      this.formIns.valuationInfo.accountNumber = this.formCol.insuranceInfo.accountNumber;
      this.formIns.valuationInfo.customerName = this.formCol.insuranceInfo.customerName;
      this.formIns.valuationInfo.customerPhone = this.resp.phoneInfo[0].phoneNumber;
      this.formIns.valuationInfo.customerEmail = this.resp.emailInfo[0].email;
      this.formIns.valuationInfo.segment = this.resp.customerInfo.segment;
      this.formIns.valuationInfo.rm = this.resp.relationManagerInfo.rmName;
      this.formIns.valuationInfo.rmCode = this.resp.relationManagerInfo.rmCode;
      this.formIns.valuationInfo.propertyOwner = this.formVal.valuationInfo.propertyOwner;
      this.formIns.valuationInfo.propertyDetails = this.formVal.valuationInfo.propertyDetails;
      this.formIns.valuationInfo.propertyOwner = this.formVal.valuationInfo.propertyOwner;
      this.formIns.valuationInfo.valuer = this.formVal.valuationInfo.valuer;
      this.formIns.valuationInfo.valCcy = this.formVal.valuationInfo.valCcy;
      this.formIns.valuationInfo.omw = this.formVal.valuationInfo.omw;
      this.formIns.valuationInfo.fsv = this.formVal.valuationInfo.fsv;
      this.formIns.valuationInfo.lcyFsv = this.formVal.valuationInfo.lcyFsv;
      this.formIns.valuationInfo.riv = this.formVal.valuationInfo.riv;
      this.formIns.valuationInfo.valuationDate = this.formVal.valuationInfo.valuationDate;
      this.formIns.valuationInfo.nextValuationDate = this.formVal.valuationInfo.nextValuationDate;
      this.formIns.valuationInfo.comments = this.formVal.valuationInfo.comments;
      this.formIns.valuationInfo.createdBy = this.username;

      if((this.formIns.insuranceInfo.insurer=='')||
      (this.formIns.insuranceInfo.insurerCover=='')||
      (this.formIns.insuranceInfo.policyNo=='')||
      (this.formIns.insuranceInfo.ccReinstate=='')||
      (this.formIns.insuranceInfo.dateIssued=='')||
      (this.formIns.insuranceInfo.dateExipred=='')||
      (this.formIns.insuranceInfo.reInstateValue=='')||
      (this.formIns.insuranceInfo.policyScheduled=='')||
      (this.formIns.insuranceInfo.policyScheduled=='')||
      (this.formIns.valuationInfo.propertyDetails==='')||
      (this.formIns.valuationInfo.valuer==='')||
      (this.formIns.valuationInfo.valCcy==='')||
      (this.formIns.valuationInfo.omw==='')||
      (this.formIns.valuationInfo.fsv==='')||
      (this.formIns.valuationInfo.lcyFsv==='')||
      (this.formIns.valuationInfo.valuationDate==='')||
      (this.formIns.valuationInfo.nextValuationDate==='')){
        toast("Please fill all required fields",3000);
      }

      else{

        if(c>=0){
        
          this.formCol.insuranceInfo.action = '';
          var ins = JSON.stringify(this.formIns);
          this.send_ins(ins);
        
        }
  
        else if(c<0){
  
          this.formCol.insuranceInfo.action = 'Exception';
          var ins = JSON.stringify(this.formIns);
          this.send_ins(ins);
        
         
        }

      }


      
     

    }

  }

  checkAccount():boolean{
    var state = true;
    var count = this.resp.accountsInfo.length;

    for (var i = 0; i < count; i++) {

      //console.log(this.accData[i].bookBalance + "----" + this.accData[i].acountDesc.includes("CURRENT") + "----" + this.accData[i].acountDesc);

      

      if((this.accData[i].bookBalance >= 0)&&(this.accData[i].acountDesc.includes("CURRENT")== true)){
        this.formCol.insuranceInfo.accountNumber = this.accData[i].accountNumber;
        this.formCol.insuranceInfo.customerName = this.accData[i].accountName;  
        this.accountType = this.accData[i].acountDesc;
        this.loading = false;	
        state = true;
        break;

      }

      else{
        this.loading = false;	
        state = false;
        
      }

    }

    return state;


    

  }

  toAccountStep(){
    
    if(this.formData.type=='Insurance'){
      this.start = false;
      this.account = true;
      this.insurance = false;
      this.valuation = false;
      this.options = false;
      this.ciftxt = false;
      this.accountStep = true;
      $("#prog").css("width", "60%");
      $("#sub").hide();
      $("#prev").show();
      $("#next").show();

    }

    else if(this.formData.type=='Valuation'){
      this.start = false;
      this.account = true;
      this.insurance = false;
      this.valuation = false;
      this.options = false;
      this.ciftxt = false;
      this.accountStep = true;
      $("#prog").css("width", "60%");
      $("#sub").hide();
      $("#prev").show();
      $("#next").show();

    }

    else if(this.formData.type=='Ins_Val'){
      this.start = false;
      this.account = true;
      this.insurance = false;
      this.valuation = false;
      this.options = false;
      this.ciftxt = false;
      this.accountStep = true;
      $("#sub").hide();
      $("#prog").css("width", "50%");
      $("#prev").show();
      $("#next").show();

    }
  }

  acoount_info(value: string){

    this.formInfoNo.accountNumber = value;
    this.formCol.insuranceInfo.customerName = "";
    this.accountType = "";
  

    var info = JSON.stringify(this.formInfoNo);
    this.api_service.account_info_no(info).subscribe(res=>{
      this.accresp = res;


      if(this.accresp.hostHeaderInfo.responseCode == "000"){	
        var count1 = this.accresp.accountsInfo.length;
        for (var i = 0; i < count1; i++) {
          if(this.accresp.accountsInfo[i].accountNumber == this.formInfoNo.accountNumber){
            this.formCol.insuranceInfo.customerName = this.accresp.accountsInfo[i].accountName;  
            this.accountType = this.accresp.accountsInfo[i].acountDesc;
            
            break;
          }
        }
      }
        
      
    });
    
    
  }


  apps(){
    var key ="collateralUserObj";
      this.storage_service.clearInfo(key);
      window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
  }
  


  // getCurrency(){
  //   this.api_service.get_currency().subscribe(res=>{
  //     this.resp2 = res;

  //     if(this.resp2.hostHeaderInfo.responseCode == "000"){

  //       var count = this.resp2.rateInfo.length;
  //       for (var i = 0; i < count; i++) {

  //           this.currency = this.resp2.rateInfo[i];
  //           console.log(this.currency);
  //       }

          
  //     }

  //   });
  // }

 
}
