import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    
  formData:any;
  loading=false;
  resp: any;
  userObj:any;
  auth=false;

  constructor(private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
    if (!this.formData) {
      this.formData = {hostHeaderInfo:{ ipAddress:'127.0.0.1', sourceChannelId:'web'} ,username: '', password: '' }
    }

    if (!this.userObj) {
      this.userObj = {
        username: '',
        fullname: ''
      }
    }
  }

  ngOnInit() {
  }

  login(){
    if(this.formData.username === ''){
      toast('Please enter your username',3000);
    }
    else if(this.formData.password === ''){
      toast('Please enter your password',3000);
      
    }
    else{
      this.loading = true;
      var data = JSON.stringify(this.formData);
      this.api_service.user_login(data).subscribe(res=>{
        this.resp = res;
        this.loading = false;
        if(this.resp.hostHeaderInfo.responseCode == "000"){
          this.userObj.fullname = this.resp.staffInfo.firstName + " " + this.resp.staffInfo.lastName;
          this.userObj.username = this.formData.username;
          this.auth = this.checkApp();
          if(this.auth==true){
            this.storage_service.saveInfo('userObj', JSON.stringify(this.userObj));
            this.router.navigate(['/dashboard']);
          }

          else{
            toast("Please contact system admin for access",3000);
          }
          
            
  
        }
        else{
          toast(this.resp.hostHeaderInfo.responseMessage.toLowerCase(),3000);
        }
      }, error => {
        toast('Oops. Please try again later',3000);
        this.loading = false;
    });
    }
  }


  checkApp():boolean{
    var state = true;
    

    if(this.resp.role){
      var count = this.resp.role.length;

      for (var i = 0; i < count; i++) {

        if(this.resp.role[i].app==="COLLATERAL"){ 
          state = true;
          break;
        }
  
        else{
          state = false;
          
        }
  
      }

    }

    else if(!this.resp.role){
      state = false;
    }
   
    return state;  

  }

}
