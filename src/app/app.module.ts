import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import  'materialize-css';
import {MaterializeModule} from 'angular2-materialize';


import { AppComponent } from './app.component';
import { FormsComponent } from './forms/forms.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { RecordsEditComponent } from './records-edit/records-edit.component';
import { RecordEditComponent } from './record-edit/record-edit.component';
@NgModule({
  declarations: [
    AppComponent,
    FormsComponent,
    DashboardComponent,
    LoginComponent,
    RecordsEditComponent,
    RecordEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2SearchPipeModule, 
    Ng2OrderModule,
    NgxPaginationModule,
    FormsModule,
    MaterializeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
