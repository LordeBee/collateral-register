import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsComponent } from './forms/forms.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RecordsEditComponent } from './records-edit/records-edit.component';
import { RecordEditComponent } from './record-edit/record-edit.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: "forms", component: FormsComponent },
  { path: "records-edit/:uuid", component: RecordsEditComponent},
  { path: "record-edit/:uuid", component: RecordEditComponent},
  { path: "dashboard", component: DashboardComponent },
  {path: "", redirectTo: "/dashboard",pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
