import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import {DatePipe} from '@angular/common';
import { toast } from 'angular2-materialize';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-records-edit',
  templateUrl: './records-edit.component.html',
  providers:[DatePipe],
  styleUrls: ['./records-edit.component.css']
})
export class RecordsEditComponent implements OnInit {
  username:any;
  sub:any;
  app_id:any;
  resp:any;
  formData:any;
  formInfo:any;
  dateshow:any;
  response:any;
  submiting = false;
  fullname:any;

  constructor(private route: ActivatedRoute, private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService,private datePipe: DatePipe) { 
    if(!this.formInfo){
      this.formInfo={
        id:''
      }
    }
    if(!this.formData){
      this.formData={
       hostHeaderInfo:{
          requestId:"C12345",
          ipAddress:"0.0.0.0",
          sourceChannelId:"WEB"
       },
       insuranceInfo:{
         id:'',
         accountNumber:'',
         customerName:'',
         customerPhone:'',
         customerEmail:'',
         currency:'',
         segment:'',
         rm:'',
         cif:'',
         rmCode:'',
         insurer:'',
         insurerCover:'',
         policyNo:'',
         annualPremium:'',
         dateIssued:'',
         dateExipred:'',
         reInstateValue:'',
         thirdParty:'',
         ccReinstate:'',
         policyScheduled:'',
         comments:'',
         action:'',
         createdBy:''
        }
      }
   
    }  
  }

  ngOnInit() {

      


    $("#step2").hide();
    $("#step3").hide();
    $("#prev").hide();
    $("#sub").hide();
    $("#ins").hide();
    $("#approve").hide();
    $("#decline").hide();

     
    $("#next").click(function(){
      var make = 1;
      if (make==1) {
      $("#step1").hide();
      $("#step3").hide();
      $("#step2").show();
      $("#ins").show();
      $("#acc").hide();
      $("#prev").show();
      $("#sub").show();
      $("#next").hide();
      $("#prog").css("width", "100%");
       }		 
    });

    $("#prev").click(function(){
      var make = 1;
      var go = 1;
      if ((make==1)&&(go==1)) {
      $("#step2").hide();
    $("#step3").hide();
    $("#step1").show(); 
    $("#prev").hide();
    $("#next").show();
    $("#sub").hide();
    $("#ins").hide();
    $("#acc").show();
    $("#prog").css("width", "50%");
    }		 
    });


    this.sub = this.route.params.subscribe(params => {
      this.app_id = params['uuid'];
      this.formInfo.id = this.app_id;
      var info = JSON.stringify(this.formInfo);

    
      this.api_service.ins_id(info).subscribe(res => {
        this.resp = res;
        if (this.resp.hostHeaderInfo.responseCode == "000") {
          this.formData.insuranceInfo.id = this.resp.insuranceInfo[0].id;
          this.formData.insuranceInfo.accountNumber = this.resp.insuranceInfo[0].accountNumber;
          this.formData.insuranceInfo.customerName = this.resp.insuranceInfo[0].customerName;
          this.formData.insuranceInfo.customerPhone = this.resp.insuranceInfo[0].customerPhone;
          this.formData.insuranceInfo.currency = this.resp.insuranceInfo[0].currency;
          this.formData.insuranceInfo.segment = this.resp.insuranceInfo[0].segment;
          this.formData.insuranceInfo.rm = this.resp.insuranceInfo[0].rm;
          this.formData.insuranceInfo.rmCode = this.resp.insuranceInfo[0].rmCode;
          this.formData.insuranceInfo.insurer = this.resp.insuranceInfo[0].insurer;
          this.formData.insuranceInfo.insurerCover = this.resp.insuranceInfo[0].insurerCover;
          this.formData.insuranceInfo.policyNo = this.resp.insuranceInfo[0].policyNo;
          this.formData.insuranceInfo.annualPremium = this.resp.insuranceInfo[0].annualPremium;
          this.formData.insuranceInfo.dateIssued = 	this.datePipe.transform(this.resp.insuranceInfo[0].dateIssued,"d-MMM-y");
          this.formData.insuranceInfo.dateExipred = this.datePipe.transform(this.resp.insuranceInfo[0].dateExipred,"d-MMM-y");
          this.formData.insuranceInfo.reInstateValue = this.resp.insuranceInfo[0].reInstateValue;
          this.formData.insuranceInfo.thirdParty = this.resp.insuranceInfo[0].thirdParty;
          this.formData.insuranceInfo.ccReinstate = this.resp.insuranceInfo[0].ccReinstate;
          this.formData.insuranceInfo.policyScheduled = this.resp.insuranceInfo[0].policyScheduled;
          this.formData.insuranceInfo.comments = this.resp.insuranceInfo[0].comments;
          this.formData.insuranceInfo.createdBy = this.resp.insuranceInfo[0].createdBy;

        
        }
        else{
          toast("Could not load data at this time" ,10000);
        }
      }, error => {
        console.log('Oops. Please try again later');
      });
     
    });
    

  


   
    var key ="collateralUserObj";
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(!userInfo){
      window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
    }
    if(userInfo != null){
      this.fullname = userInfo.fullname;
      this.username = userInfo.username;
      }

  }

  form(){
    this.router.navigate(['/forms']);
  }

  logout(){
    var key ="collateralUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }

  change(){
    if((this.formData.insuranceInfo.insurer!==this.resp.insuranceInfo[0].insurer)
      || (this.formData.insuranceInfo.insurerCover!==this.resp.insuranceInfo[0].insurerCover)
      || (this.formData.insuranceInfo.policyNo!==this.resp.insuranceInfo[0].policyNo)
      || (this.formData.insuranceInfo.annualPremium!==this.resp.insuranceInfo[0].annualPremium)
      || (this.formData.insuranceInfo.reInstateValue!==this.resp.insuranceInfo[0].reInstateValue)
      || (this.formData.insuranceInfo.thirdParty!==this.resp.insuranceInfo[0].thirdParty)
      || (this.formData.insuranceInfo.ccReinstate!==this.resp.insuranceInfo[0].ccReinstate)
      || (this.formData.insuranceInfo.policyScheduled!==this.resp.insuranceInfo[0].policyScheduled)
      || (this.formData.insuranceInfo.comments!==this.resp.insuranceInfo[0].comments)
      || (this.formData.insuranceInfo.dateIssued!==this.datePipe.transform(this.resp.insuranceInfo[0].dateIssued,"d-MMM-y"))
      || (this.formData.insuranceInfo.dateExipred!==this.datePipe.transform(this.resp.insuranceInfo[0].dateExipred,"d-MMM-y"))
      ){
      jQuery('.modal').modal(
        {
          dismissible: true,
          opacity: .5,
          inDuration: 300,
          outDuration: 200,
          startingTop: '1%',
          endingTop: '10%',
        }
      );
      jQuery('.confirmModal').modal('open');
      
  
    }
   
    else{
        toast("No changes made to submit",3000);
      }
    }

    update(data){
        this.api_service.col_updata(data).subscribe(res=>{
        this.response = res;
  
        if(this.response.hostHeaderInfo.responseCode == "000"){
          toast("Changes submitted successfully",3000);
          jQuery('.confirmModal').modal('close');
          this.router.navigate(['/dashboard']);
  
        }
  
        else{
          toast("Could not submit at this time",3000);
          jQuery('.confirmModal').modal('close');
      
        }
       
       
        }, error => {
          toast("Sorry, Couldn't submit at this time",3000);
    
       
      });
    }

    submitChanges(){
      this.submiting = true;
      $("#send").hide();
      $("#dec").hide();
      if(this.formData.insuranceInfo.reInstateValue>=0){
        this.formData.insuranceInfo.action = "";
        var data = JSON.stringify(this.formData);
        this.update(data);
      }

      else if(this.formData.insuranceInfo.reInstateValue<0){
        this.formData.insuranceInfo.action = "Exception";
        var data = JSON.stringify(this.formData);
        this.update(data);
      }


      
     

     

    }  

    apps(){
      var key ="collateralUserObj";
        this.storage_service.clearInfo(key);
        window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
    }
    
    getData(){
      if(this.formData.insuranceInfo.insurerCover!==this.resp.insuranceInfo[0].insurerCover){
        var a = parseFloat(this.formData.insuranceInfo.insurerCover);
        var b = parseFloat(this.formData.insuranceInfo.ccReinstate);
        var c = a-b;
      this.formData.insuranceInfo.reInstateValue = c;
      }
    }

    getccData(){
      var a = parseFloat(this.formData.insuranceInfo.insurerCover);
        var b = parseFloat(this.formData.insuranceInfo.ccReinstate);
        var c = a-b;
        this.formData.insuranceInfo.reInstateValue = c;
    }

    dashboard(){
      this.router.navigate(['/dashboard']);
    }



  

}
