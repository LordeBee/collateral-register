import { Component, OnInit } from '@angular/core';
import { StorageServiceService } from '../service/storage-service.service';
import { Router } from '@angular/router';
import { ApiServiceService } from '../service/api-service.service';
import { TitleCasePipe } from '@angular/common';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { toast } from 'angular2-materialize';

declare var jQuery:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  username:any; 
  formData:any;
  formType:any;
  resp:any;
  checkercomm:any;
  response:any;
  type:any;
  valuationList = false;
  insuranceList = false;
  error = false;
  valInfo:any;
  insInfo:any;
  isData:any;
  Loading = false;
  fullname:any;
  

  constructor(private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
    if (!this.formData) {
      this.formData = {
        action: ''		
       }
      }
      if (!this.formType) {
        this.formType = {
          col: ''		
         }
        }  

     
  }

  ngOnInit() {

    this.insuranceList = true;
    
    
    this.formData.action='Expired';
    this.formType.col='Insurance';
    this.Loading = true;
    var info = JSON.stringify(this.formData);
        this.api_service.ins_info(info).subscribe(res=>{
          this.response = res;

          if(this.response.hostHeaderInfo.responseCode === "000"){
            this.insInfo = this.response.insuranceInfo;
            this.isData = true;
            this.Loading = false;
           
          }
          else{
            
            this.insuranceList = false;
            this.isData = false;
            this.Loading = false;
            
  
          }
          }, error => {
            this.isData = false;
            this.Loading = false;
      
         
        });;
    // this.loadDec();
    var key ="collateralUserObj";
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if(!userInfo){
      window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
    }
    if(userInfo != null){
      this.fullname = userInfo.fullname;
      this.username = userInfo.username;
      }

  
  }

  goto_Edit(event: Event) {
    let elementId: string = (event.target as Element).id;
    this.router.navigate(['/records-edit', elementId]);
    // alert(elementId);
  }

  go_Edit(event: Event) {
    let elementId: string = (event.target as Element).id;
    this.router.navigate(['/record-edit', elementId]);
    // alert(elementId);
  }

  form(){
    this.router.navigate(['/forms']);
  }

  logout(){
    var key ="collateralUserObj";
    this.storage_service.clearInfo(key);
    this.storage_service.clearInfo('appsData');
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/login';
  }

 

  view(){
    this.router.navigate(['/records']);
  }

  
  
  collateral_type(value: string) {
    if((value=='Insurance')&&((this.formData.action=='Expired')||(this.formData.action=='Current'))){
     

      this.get_Ins_Data();
    }
    else if((value=='Valuation')&&((this.formData.action=='Expired')||(this.formData.action=='Current'))){
      

     this.get_Val_Data();
    }

    else if((value=='Valuation')&&(this.formData.action=='EXCEPTION')) {
      this.insuranceList = true;
      this.valuationList = false;
      this.isData = false;
     }

    else if((value=='Insurance')&&(this.formData.action=='EXCEPTION')){
     

     this.get_Ins_Data();
    }
    

  }


  collateral_action(value: string) {
   
    
    if((value=='EXCEPTION')&&((this.type=='Insurance')||(this.formType.col=='Insurance'))) {
      

      this.get_Ins_Data();
      
    }

    else if((value=='EXCEPTION')&&((this.type=='Valuation')||(this.formType.col=='Valuation'))) {
      this.insuranceList = true;
      this.valuationList = false;
      this.isData = false;
     }

    else if((value=='Expired')&&((this.type=='Insurance')||(this.formType.col=='Insurance'))) {  
      

      this.get_Ins_Data();
      
    }  

    else if((value=='Expired')&&((this.type=='Valuation')||(this.formType.col=='Valuation'))) {  
      
      

      this.get_Val_Data();
    }  

    else if((value=='Current')&&((this.type=='Insurance')||(this.formType.col=='Insurance'))) {
      this.insuranceList = true;
      this.valuationList = false;

      this.get_Ins_Data();
    }  

    else if((value=='Current')&&((this.type=='Valuation')||(this.formType.col=='Valuation'))) {
      
      this.get_Val_Data();
    } 
}

get_Val_Data(){
  this.Loading = true;
  var info = JSON.stringify(this.formData);
      this.api_service.val_info(info).subscribe(res=>{
        this.response = res;
        if(this.response.hostHeaderInfo.responseCode == "000"){
          this.insuranceList = false;
          this.valuationList = true;
          this.valInfo = this.response.valuationInfo;
          this.isData = true;
          this.Loading = false;
        
        }
        else{
          
          this.valuationList = false;
          this.isData = false;
          this.Loading = false;

        }
        }, error => {
          this.isData = false;
          this.Loading = false;
    
       
      });
}

get_Ins_Data(){
  this.Loading = true;
  var info = JSON.stringify(this.formData);
      this.api_service.ins_info(info).subscribe(res=>{
        this.response = res;
        if(this.response.hostHeaderInfo.responseCode === "000"){
          this.insInfo = this.response.insuranceInfo;
          this.isData = true;
          this.Loading = false;
         
        }
        else{
          
          this.insuranceList = false;
          this.isData = false;
          this.Loading = false;
          

        }
        }, error => {
          this.isData = false;
          this.Loading = false;
    
       
      });
}

refresh_Table($event){
  this.Loading = true;
  if(this.formType.col=='Valuation'){
    this.get_Val_Data();
  }
  else if(this.formType.col=='Insurance'){
    this.get_Ins_Data();
  }
  event.preventDefault();
}


apps(){
  var key ="collateralUserObj";
    this.storage_service.clearInfo(key);
    window.location.href = 'https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/apps';
}



}
 